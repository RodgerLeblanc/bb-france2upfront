<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>about</name>
    <message>
        <location filename="../assets/about.qml" line="5"/>
        <source>About</source>
        <translation>À Propos</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="23"/>
        <source>This app updates UpFront&apos; Active Frame in the background (headless). Click on the icon to download UpFront</source>
        <translation>Cette app communique avec UpFront en arrière-plan (headless). Clickez sur l&apos;icone pour télécharger UpFront</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="41"/>
        <source>Developped by
Roger Leblanc
Version: </source>
        <translation>Développé par
Roger Leblanc
Version: </translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="54"/>
        <source>Contact dev :</source>
        <translation>Pour infos :</translation>
    </message>
    <message>
        <location filename="../assets/about.qml" line="92"/>
        <source>Other apps from this dev :</source>
        <translation>Autres apps de ce dev :</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="55"/>
        <source>About</source>
        <translation>À Propos</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="109"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <location filename="../assets/settings.qml" line="5"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../assets/settings.qml" line="37"/>
        <source>Choose the text color in UpFront</source>
        <translation>Couleur du texte dans UpFront</translation>
    </message>
</context>
</TS>
